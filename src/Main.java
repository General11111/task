public class Main {

    public static void main(String[] args) {

        String max = null;
        int iTemp = 0;
        int iMax = 0;

        String[] myArray  = new String[] {
                "Полезные методы для работы с массивами",
                "Вывести элементы массива на экран",
                "Полезные методы для работы с массивами",
                "Массив строк",
                "Вывести элементы массива на экран",
                "Полезные методы для работы с массивами",
                "Номер",
                "Полезные методы для работы с массивами"};

        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray.length; j++) {
                if (myArray[i].equals(myArray[j])) {
                    iTemp++;
                }
            }
            if(iTemp > iMax){
                iMax = iTemp;
                max = myArray[i];
            }
            iTemp = 0;
        }
        System.out.println("Чаще всего встречается строка: " + max);
        System.out.println("Количество вывода строк: " +  iMax);

        int count = 1;
        for (int i = 0; i < max.length(); i++) {
            if(max.charAt(i) == ' '){
                count++;
            }
        }
        System.out.println("Количество слов в строке: " + count);
    }
}
